if(isset($_COOKIE['cookname']) && isset($_COOKIE['cookpass'])){
$_SESSION['login_user'] = $_COOKIE['cookname'];
$_SESSION['login_pw'] = $_COOKIE['cookpass'];


<script>
    $(function () {
        $('#terms').change(function () {
            if ($(this).is(':checked')) {
                $('#loglog').removeAttr('disabled');
            }
            else {
                $('#loglog').attr('disabled', 'disabled').attr('placeholder', 'disabled')
                $('#lalog').attr('class','input state-disabled');
            }
        });
    });
</script>









<fieldset>
    <section><label><strong>Total no. of captive treatment equipment installed by the HCFs (i.e excluding Common Bio Medical Waste Treatment and Disposal Facility (CBMWTF))</strong></label></section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberapcd1" type="text" name="numberapcd1" placeholder="No. of Incinerators with APCD">
            <b class="tooltip tooltip-bottom-right">APCD stands for Air-Pollution control device </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberwapcd1" type="text" name="numberwapcd1" placeholder="No. of Incinerators without APCD">
            <b class="tooltip tooltip-bottom-right">APCD stands for Air-Pollution control device </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberaclave1" type="text" name="numberaclave1" placeholder="No. of Autoclave">
            <b class="tooltip tooltip-bottom-right">Enter Total Number of Autoclaves </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numbermw1" type="text" name="numbermw1" placeholder="No. of Microwave">
            <b class="tooltip tooltip-bottom-right">Enter Total Number of Microwave </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberhc1" type="text" name="numberhc1" placeholder="No. of Hydroclave">
            <b class="tooltip tooltip-bottom-right">Enter Total Number of Hydroclaves</b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numbersh1" type="text" name="numbersh1" placeholder="No. of Shredder">
            <b class="tooltip tooltip-bottom-right">Enter Total Number of Shredder</b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberdeep1" type="text" name="numberdeep1" placeholder="No. of Deep Burial">
            <b class="tooltip tooltip-bottom-right">Enter Total Number of Deep Burial</b>
        </label>
    </section>

</fieldset>

<fieldset>

    <section><label><strong>No. of CBWTF (Common Bio Medical Waste Treatment and Disposal Facility)</strong></label></section>

    <section>
        <label class="select">
            <select name="gender">
                <option value="0" selected disabled>Please specify if a hospital treatment facility is also used by other HCF</option>
                <option value="Yes">Yes</option>
                <option value="No">No</option>
                <b class="tooltip tooltip-bottom-right">Please select your Gender</b>
            </select>
            <i></i>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberinop1" type="text" name="numberinop1" placeholder="No. of CBWTF in Operation">
            <b class="tooltip tooltip-bottom-right">CBWTF stands for Common Bio Medical Waste Treatment and Disposal Facility</b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberuc1" type="text" name="numberuc1" placeholder="No. of CBWTF Under Construction">
            <b class="tooltip tooltip-bottom-right">CBWTF stands for Common Bio Medical Waste Treatment and Disposal Facility</b>
        </label>
    </section>

</fieldset>

<fieldset>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberhcfcbwtf1" type="text" name="numberhcfcbwtf1" placeholder="No. of HCFs which are utilising CBWTF's">
            <b class="tooltip tooltip-bottom-right">CBWTF stands for Common Bio Medical Waste Treatment and Disposal Facility, HCF stand for Hospital and Nursing Homes </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberbmw1" type="text" name="numberbmw1" placeholder="Total quantity of BMW generated (kg/day)">
            <b class="tooltip tooltip-bottom-right">BMW stands for Bio Medical Waste</b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numbertbmw1" type="text" name="numbertbmw1" placeholder="Total quantity of BMW treated (kg/day)">
            <b class="tooltip tooltip-bottom-right">BMW stands for Bio Medical Waste</b>
        </label>
    </section>

</fieldset>

<fieldset>
    <section><label><strong>No. of Facilities violated BMW Rules</strong></label></section>
    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberviohcf1" type="text" name="numberviohcf1" placeholder="No. of Facilities violated BMW Rules For HCF's">
            <b class="tooltip tooltip-bottom-right">HCF stand for Hospital and Nursing Homes </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberviocbwtf1" type="text" name="numberviocbwtf1" placeholder="No. of Facilities violated BMW Rules For CBWTF's">
            <b class="tooltip tooltip-bottom-right">CBWTF stands for Common Bio Medical Waste Treatment and Disposal Facility</b>
        </label>
    </section>

</fieldset>


<fieldset>

    <section><label><strong>Total No. of Show Cause Notices/Directions issued to defaulter Facilities</strong></label></section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberschcf1" type="text" name="numberschcf1" placeholder="Total No. of Show Cause Notices/Directions issued to defaulter Facilities For HCF's">
            <b class="tooltip tooltip-bottom-right">HCF stand for Hospital and Nursing Homes </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numbersccbwtf1" type="text" name="numbersccbwtf1" placeholder="Total No. of Show Cause Notices/Directions issued to defaulter Facilities For CBWTF's">
            <b class="tooltip tooltip-bottom-right">CBWTF stands for Common Bio Medical Waste Treatment and Disposal Facility</b>
        </label>
    </section>

</fieldset>

<fieldset>
    <br><br><br>
</fieldset>

<!-------------------------------------- PART 2 ------------------------------------------------------------->

<fieldset>
    <section>
        <label class="checkbox"><input id="a2" type="checkbox" name="terms"  value="accept" checked><i></i>Hospitals & Nursing Homes in town with <strong> population of below 30 Lakhs <em><span style="text-decoration: underline;">with 500 beds and above</span></em></strong> <em>(uncheck if not applicable)</em></label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberhcf2" type="text" name="numberhcf2" placeholder="Total no. of  HCFs irrespective of no. of patients treated">
            <b class="tooltip tooltip-bottom-right">HCF stands for Hospitals & Nursing Homes </b>
        </label>
    </section>


    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberbeds2" type="text" name="numberbeds2" placeholder="Total no. of Beds">
            <b class="tooltip tooltip-bottom-right">Enter Total Number of Beds </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberra2" type="text" name="numberra2" placeholder="No. of HCF's required to take Authorisation">
            <b class="tooltip tooltip-bottom-right">HCF stands for Hospitals & Nursing Homes </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberga2" type="text" name="numberga2" placeholder="No. of HCf's Granted Authorisation">
            <b class="tooltip tooltip-bottom-right">HCF stands for Hospitals & Nursing Homes </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberotf2" type="text" name="numberotf2" placeholder="No. of HCF's having own treatment and disposal facilities">
            <b class="tooltip tooltip-bottom-right">HCF stands for Hospitals & Nursing Homes </b>
        </label>
    </section>
</fieldset>

<fieldset>
    <section><label><strong>Total no. of captive treatment equipment installed by the HCFs (i.e excluding Common Bio Medical Waste Treatment and Disposal Facility (CBMWTF))</strong></label></section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberapcd2" type="text" name="numberapcd2" placeholder="No. of Incinerators with APCD">
            <b class="tooltip tooltip-bottom-right">APCD stands for Air-Pollution control device </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberwapcd2" type="text" name="numberwapcd2" placeholder="No. of Incinerators without APCD">
            <b class="tooltip tooltip-bottom-right">APCD stands for Air-Pollution control device </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberaclave2" type="text" name="numberaclave2" placeholder="No. of Autoclave">
            <b class="tooltip tooltip-bottom-right">Enter Total Number of Autoclaves </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numbermw2" type="text" name="numbermw2" placeholder="No. of Microwave">
            <b class="tooltip tooltip-bottom-right">Enter Total Number of Microwave </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberhc2" type="text" name="numberhc2" placeholder="No. of Hydroclave">
            <b class="tooltip tooltip-bottom-right">Enter Total Number of Hydroclaves</b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numbersh2" type="text" name="numbersh2" placeholder="No. of Shredder">
            <b class="tooltip tooltip-bottom-right">Enter Total Number of Shredder</b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberdeep2" type="text" name="numberdeep2" placeholder="No. of Deep Burial">
            <b class="tooltip tooltip-bottom-right">Enter Total Number of Deep Burial</b>
        </label>
    </section>

</fieldset>

<fieldset>

    <section><label><strong>No. of CBWTF (Common Bio Medical Waste Treatment and Disposal Facility)</strong></label></section>

    <section>
        <label class="select">
            <select id="opt2" name="opt2">
                <option value="0" selected disabled>Please specify if a hospital treatment facility is also used by other HCF</option>
                <option value="Yes">Yes</option>
                <option value="No">No</option>
                <b class="tooltip tooltip-bottom-right">Please select your Gender</b>
            </select>
            <i></i>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberinop2" type="text" name="numberinop2" placeholder="No. of CBWTF in Operation">
            <b class="tooltip tooltip-bottom-right">CBWTF stands for Common Bio Medical Waste Treatment and Disposal Facility</b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberuc2" type="text" name="numberuc2" placeholder="No. of CBWTF Under Construction">
            <b class="tooltip tooltip-bottom-right">CBWTF stands for Common Bio Medical Waste Treatment and Disposal Facility</b>
        </label>
    </section>

</fieldset>

<fieldset>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberhcfcbwtf2" type="text" name="numberhcfcbwtf2" placeholder="No. of HCFs which are utilising CBWTF's">
            <b class="tooltip tooltip-bottom-right">CBWTF stands for Common Bio Medical Waste Treatment and Disposal Facility, HCF stand for Hospital and Nursing Homes </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberbmw2" type="text" name="numberbmw2" placeholder="Total quantity of BMW generated (kg/day)">
            <b class="tooltip tooltip-bottom-right">BMW stands for Bio Medical Waste</b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numbertbmw2" type="text" name="numbertbmw2" placeholder="Total quantity of BMW treated (kg/day)">
            <b class="tooltip tooltip-bottom-right">BMW stands for Bio Medical Waste</b>
        </label>
    </section>

</fieldset>

<fieldset>
    <section><label><strong>No. of Facilities violated BMW Rules</strong></label></section>
    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberviohcf2" type="text" name="numberviohcf2" placeholder="No. of Facilities violated BMW Rules For HCF's">
            <b class="tooltip tooltip-bottom-right">HCF stand for Hospital and Nursing Homes </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberviocbwtf2" type="text" name="numberviocbwtf2" placeholder="No. of Facilities violated BMW Rules For CBWTF's">
            <b class="tooltip tooltip-bottom-right">CBWTF stands for Common Bio Medical Waste Treatment and Disposal Facility</b>
        </label>
    </section>

</fieldset>


<fieldset>

    <section><label><strong>Total No. of Show Cause Notices/Directions issued to defaulter Facilities</strong></label></section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberschcf2" type="text" name="numberschcf2" placeholder="Total No. of Show Cause Notices/Directions issued to defaulter Facilities For HCF's">
            <b class="tooltip tooltip-bottom-right">HCF stand for Hospital and Nursing Homes </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numbersccbwtf2" type="text" name="numbersccbwtf2" placeholder="Total No. of Show Cause Notices/Directions issued to defaulter Facilities For CBWTF's">
            <b class="tooltip tooltip-bottom-right">CBWTF stands for Common Bio Medical Waste Treatment and Disposal Facility</b>
        </label>
    </section>

</fieldset>

<fieldset>
    <br><br><br>
</fieldset>

<!-------------------------------------- PART 2 Ends Here --------------------------------------------------->


<!-------------------------------------- PART 3 ------------------------------------------------------------->

<fieldset>
    <section>
        <label class="checkbox"><input type="checkbox" name="terms" id="a3" value="accept" checked><i></i>Hospitals & Nursing Homes in town with <strong> population of below 30 Lakhs <em><span style="text-decoration: underline;">with 200 beds and above but less than 500 beds </span></em></strong> <em>(uncheck if not applicable)</em></label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberhcf3" type="text" name="numberhcf3" placeholder="Total no. of  HCFs irrespective of no. of patients treated">
            <b class="tooltip tooltip-bottom-right">HCF stands for Hospitals & Nursing Homes </b>
        </label>
    </section>


    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberbeds3" type="text" name="numberbeds3" placeholder="Total no. of Beds">
            <b class="tooltip tooltip-bottom-right">Enter Total Number of Beds </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberra3" type="text" name="numberra3" placeholder="No. of HCF's required to take Authorisation">
            <b class="tooltip tooltip-bottom-right">HCF stands for Hospitals & Nursing Homes </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberga3" type="text" name="numberga3" placeholder="No. of HCf's Granted Authorisation">
            <b class="tooltip tooltip-bottom-right">HCF stands for Hospitals & Nursing Homes </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberotf3" type="text" name="numberotf3" placeholder="No. of HCF's having own treatment and disposal facilities">
            <b class="tooltip tooltip-bottom-right">HCF stands for Hospitals & Nursing Homes </b>
        </label>
    </section>
</fieldset>

<fieldset>
    <section><label><strong>Total no. of captive treatment equipment installed by the HCFs (i.e excluding Common Bio Medical Waste Treatment and Disposal Facility (CBMWTF))</strong></label></section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberapcd3" type="text" name="numberapcd3" placeholder="No. of Incinerators with APCD">
            <b class="tooltip tooltip-bottom-right">APCD stands for Air-Pollution control device </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberwapcd3" type="text" name="numberwapcd3" placeholder="No. of Incinerators without APCD">
            <b class="tooltip tooltip-bottom-right">APCD stands for Air-Pollution control device </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberaclave3" type="text" name="numberaclave3" placeholder="No. of Autoclave">
            <b class="tooltip tooltip-bottom-right">Enter Total Number of Autoclaves </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numbermw3" type="text" name="numbermw3" placeholder="No. of Microwave">
            <b class="tooltip tooltip-bottom-right">Enter Total Number of Microwave </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberhc3" type="text" name="numberhc3" placeholder="No. of Hydroclave">
            <b class="tooltip tooltip-bottom-right">Enter Total Number of Hydroclaves</b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numbersh3" type="text" name="numbersh3" placeholder="No. of Shredder">
            <b class="tooltip tooltip-bottom-right">Enter Total Number of Shredder</b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberdeep3" type="text" name="numberdeep3" placeholder="No. of Deep Burial">
            <b class="tooltip tooltip-bottom-right">Enter Total Number of Deep Burial</b>
        </label>
    </section>

</fieldset>

<fieldset>

    <section><label><strong>No. of CBWTF (Common Bio Medical Waste Treatment and Disposal Facility)</strong></label></section>

    <section>
        <label class="select">
            <select name="gender">
                <option value="0" selected disabled>Please specify if a hospital treatment facility is also used by other HCF</option>
                <option value="Yes">Yes</option>
                <option value="No">No</option>
                <b class="tooltip tooltip-bottom-right">Please select your Gender</b>
            </select>
            <i></i>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberinop3" type="text" name="numberinop3" placeholder="No. of CBWTF in Operation">
            <b class="tooltip tooltip-bottom-right">CBWTF stands for Common Bio Medical Waste Treatment and Disposal Facility</b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberuc3" type="text" name="numberuc3" placeholder="No. of CBWTF Under Construction">
            <b class="tooltip tooltip-bottom-right">CBWTF stands for Common Bio Medical Waste Treatment and Disposal Facility</b>
        </label>
    </section>

</fieldset>

<fieldset>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberhcfcbwtf3" type="text" name="numberhcfcbwtf3" placeholder="No. of HCFs which are utilising CBWTF's">
            <b class="tooltip tooltip-bottom-right">CBWTF stands for Common Bio Medical Waste Treatment and Disposal Facility, HCF stand for Hospital and Nursing Homes </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberbmw3" type="text" name="numberbmw3" placeholder="Total quantity of BMW generated (kg/day)">
            <b class="tooltip tooltip-bottom-right">BMW stands for Bio Medical Waste</b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numbertbmw3" type="text" name="numbertbmw3" placeholder="Total quantity of BMW treated (kg/day)">
            <b class="tooltip tooltip-bottom-right">BMW stands for Bio Medical Waste</b>
        </label>
    </section>

</fieldset>

<fieldset>
    <section><label><strong>No. of Facilities violated BMW Rules</strong></label></section>
    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberviohcf3" type="text" name="numberviohcf3" placeholder="No. of Facilities violated BMW Rules For HCF's">
            <b class="tooltip tooltip-bottom-right">HCF stand for Hospital and Nursing Homes </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberviocbwtf3" type="text" name="numberviocbwtf3" placeholder="No. of Facilities violated BMW Rules For CBWTF's">
            <b class="tooltip tooltip-bottom-right">CBWTF stands for Common Bio Medical Waste Treatment and Disposal Facility</b>
        </label>
    </section>

</fieldset>


<fieldset>

    <section><label><strong>Total No. of Show Cause Notices/Directions issued to defaulter Facilities</strong></label></section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberschcf3" type="text" name="numberschcf3" placeholder="Total No. of Show Cause Notices/Directions issued to defaulter Facilities For HCF's">
            <b class="tooltip tooltip-bottom-right">HCF stand for Hospital and Nursing Homes </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numbersccbwtf3" type="text" name="numbersccbwtf3" placeholder="Total No. of Show Cause Notices/Directions issued to defaulter Facilities For CBWTF's">
            <b class="tooltip tooltip-bottom-right">CBWTF stands for Common Bio Medical Waste Treatment and Disposal Facility</b>
        </label>
    </section>

</fieldset>

<fieldset>
    <br><br><br>
</fieldset>

<!-------------------------------------- PART 3 Ends Here --------------------------------------------------->

<!-------------------------------------- PART 4 ------------------------------------------------------------->

<fieldset>
    <section>
        <label class="checkbox"><input type="checkbox" name="terms" id="a4" value="accept" checked><i></i>Hospitals & Nursing Homes in town with <strong> population of below 30 Lakhs <em><span style="text-decoration: underline;">with 50 beds and above but less than 200 beds </span></em></strong> <em>(uncheck if not applicable)</em></label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberhcf4" type="text" name="numberhcf4" placeholder="Total no. of  HCFs irrespective of no. of patients treated">
            <b class="tooltip tooltip-bottom-right">HCF stands for Hospitals & Nursing Homes </b>
        </label>
    </section>


    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberbeds4" type="text" name="numberbeds4" placeholder="Total no. of Beds">
            <b class="tooltip tooltip-bottom-right">Enter Total Number of Beds </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberra4" type="text" name="numberra4" placeholder="No. of HCF's required to take Authorisation">
            <b class="tooltip tooltip-bottom-right">HCF stands for Hospitals & Nursing Homes </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberga4" type="text" name="numberga4" placeholder="No. of HCf's Granted Authorisation">
            <b class="tooltip tooltip-bottom-right">HCF stands for Hospitals & Nursing Homes </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberotf4" type="text" name="numberotf4" placeholder="No. of HCF's having own treatment and disposal facilities">
            <b class="tooltip tooltip-bottom-right">HCF stands for Hospitals & Nursing Homes </b>
        </label>
    </section>
</fieldset>

<fieldset>
    <section><label><strong>Total no. of captive treatment equipment installed by the HCFs (i.e excluding Common Bio Medical Waste Treatment and Disposal Facility (CBMWTF))</strong></label></section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberapcd4" type="text" name="numberapcd4" placeholder="No. of Incinerators with APCD">
            <b class="tooltip tooltip-bottom-right">APCD stands for Air-Pollution control device </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberwapcd4" type="text" name="numberwapcd4" placeholder="No. of Incinerators without APCD">
            <b class="tooltip tooltip-bottom-right">APCD stands for Air-Pollution control device </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberaclave4" type="text" name="numberaclave4" placeholder="No. of Autoclave">
            <b class="tooltip tooltip-bottom-right">Enter Total Number of Autoclaves </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numbermw4" type="text" name="numbermw4" placeholder="No. of Microwave">
            <b class="tooltip tooltip-bottom-right">Enter Total Number of Microwave </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberhc4" type="text" name="numberhc4" placeholder="No. of Hydroclave">
            <b class="tooltip tooltip-bottom-right">Enter Total Number of Hydroclaves</b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numbersh4" type="text" name="numbersh4" placeholder="No. of Shredder">
            <b class="tooltip tooltip-bottom-right">Enter Total Number of Shredder</b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberdeep4" type="text" name="numberdeep4" placeholder="No. of Deep Burial">
            <b class="tooltip tooltip-bottom-right">Enter Total Number of Deep Burial</b>
        </label>
    </section>

</fieldset>

<fieldset>

    <section><label><strong>No. of CBWTF (Common Bio Medical Waste Treatment and Disposal Facility)</strong></label></section>

    <section>
        <label class="select">
            <select name="gender">
                <option value="0" selected disabled>Please specify if a hospital treatment facility is also used by other HCF</option>
                <option value="Yes">Yes</option>
                <option value="No">No</option>
                <b class="tooltip tooltip-bottom-right">Please select your Gender</b>
            </select>
            <i></i>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberinop4" type="text" name="numberinop4" placeholder="No. of CBWTF in Operation">
            <b class="tooltip tooltip-bottom-right">CBWTF stands for Common Bio Medical Waste Treatment and Disposal Facility</b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberuc4" type="text" name="numberuc4" placeholder="No. of CBWTF Under Construction">
            <b class="tooltip tooltip-bottom-right">CBWTF stands for Common Bio Medical Waste Treatment and Disposal Facility</b>
        </label>
    </section>

</fieldset>

<fieldset>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberhcfcbwtf4" type="text" name="numberhcfcbwtf4" placeholder="No. of HCFs which are utilising CBWTF's">
            <b class="tooltip tooltip-bottom-right">CBWTF stands for Common Bio Medical Waste Treatment and Disposal Facility, HCF stand for Hospital and Nursing Homes </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberbmw4" type="text" name="numberbmw4" placeholder="Total quantity of BMW generated (kg/day)">
            <b class="tooltip tooltip-bottom-right">BMW stands for Bio Medical Waste</b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numbertbmw4" type="text" name="numbertbmw4" placeholder="Total quantity of BMW treated (kg/day)">
            <b class="tooltip tooltip-bottom-right">BMW stands for Bio Medical Waste</b>
        </label>
    </section>

</fieldset>

<fieldset>
    <section><label><strong>No. of Facilities violated BMW Rules</strong></label></section>
    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberviohcf4" type="text" name="numberviohcf4" placeholder="No. of Facilities violated BMW Rules For HCF's">
            <b class="tooltip tooltip-bottom-right">HCF stand for Hospital and Nursing Homes </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberviocbwtf4" type="text" name="numberviocbwtf4" placeholder="No. of Facilities violated BMW Rules For CBWTF's">
            <b class="tooltip tooltip-bottom-right">CBWTF stands for Common Bio Medical Waste Treatment and Disposal Facility</b>
        </label>
    </section>

</fieldset>


<fieldset>

    <section><label><strong>Total No. of Show Cause Notices/Directions issued to defaulter Facilities</strong></label></section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberschcf4" type="text" name="numberschcf4" placeholder="Total No. of Show Cause Notices/Directions issued to defaulter Facilities For HCF's">
            <b class="tooltip tooltip-bottom-right">HCF stand for Hospital and Nursing Homes </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numbersccbwtf4" type="text" name="numbersccbwtf4" placeholder="Total No. of Show Cause Notices/Directions issued to defaulter Facilities For CBWTF's">
            <b class="tooltip tooltip-bottom-right">CBWTF stands for Common Bio Medical Waste Treatment and Disposal Facility</b>
        </label>
    </section>

</fieldset>

<fieldset>
    <br><br><br>
</fieldset>

<!-------------------------------------- PART 4 Ends Here --------------------------------------------------->

<!-------------------------------------- PART 5 ------------------------------------------------------------->

<fieldset>
    <section>
        <label class="checkbox"><input type="checkbox" name="terms" id="a5" value="accept" checked><i></i>Hospitals & Nursing Homes in town with <strong> population of below 30 Lakhs <em><span style="text-decoration: underline;">with less than 50 beds </span></em></strong> <em>(uncheck if not applicable)</em></label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberhcf5" type="text" name="numberhcf5" placeholder="Total no. of  HCFs irrespective of no. of patients treated">
            <b class="tooltip tooltip-bottom-right">HCF stands for Hospitals & Nursing Homes </b>
        </label>
    </section>


    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberbeds5" type="text" name="numberbeds5" placeholder="Total no. of Beds">
            <b class="tooltip tooltip-bottom-right">Enter Total Number of Beds </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberra5" type="text" name="numberra5" placeholder="No. of HCF's required to take Authorisation">
            <b class="tooltip tooltip-bottom-right">HCF stands for Hospitals & Nursing Homes </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberga5" type="text" name="numberga5" placeholder="No. of HCf's Granted Authorisation">
            <b class="tooltip tooltip-bottom-right">HCF stands for Hospitals & Nursing Homes </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberotf5" type="text" name="numberotf5" placeholder="No. of HCF's having own treatment and disposal facilities">
            <b class="tooltip tooltip-bottom-right">HCF stands for Hospitals & Nursing Homes </b>
        </label>
    </section>
</fieldset>

<fieldset>
    <section><label><strong>Total no. of captive treatment equipment installed by the HCFs (i.e excluding Common Bio Medical Waste Treatment and Disposal Facility (CBMWTF))</strong></label></section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberapcd5" type="text" name="numberapcd5" placeholder="No. of Incinerators with APCD">
            <b class="tooltip tooltip-bottom-right">APCD stands for Air-Pollution control device </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberwapcd5" type="text" name="numberwapcd5" placeholder="No. of Incinerators without APCD">
            <b class="tooltip tooltip-bottom-right">APCD stands for Air-Pollution control device </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberaclave5" type="text" name="numberaclave5" placeholder="No. of Autoclave">
            <b class="tooltip tooltip-bottom-right">Enter Total Number of Autoclaves </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numbermw5" type="text" name="numbermw5" placeholder="No. of Microwave">
            <b class="tooltip tooltip-bottom-right">Enter Total Number of Microwave </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberhc5" type="text" name="numberhc5" placeholder="No. of Hydroclave">
            <b class="tooltip tooltip-bottom-right">Enter Total Number of Hydroclaves</b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numbersh5" type="text" name="numbersh5" placeholder="No. of Shredder">
            <b class="tooltip tooltip-bottom-right">Enter Total Number of Shredder</b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberdeep5" type="text" name="numberdeep5" placeholder="No. of Deep Burial">
            <b class="tooltip tooltip-bottom-right">Enter Total Number of Deep Burial</b>
        </label>
    </section>

</fieldset>

<fieldset>

    <section><label><strong>No. of CBWTF (Common Bio Medical Waste Treatment and Disposal Facility)</strong></label></section>

    <section>
        <label class="select">
            <select name="gender">
                <option value="0" selected disabled>Please specify if a hospital treatment facility is also used by other HCF</option>
                <option value="Yes">Yes</option>
                <option value="No">No</option>
                <b class="tooltip tooltip-bottom-right">Please select your Gender</b>
            </select>
            <i></i>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberinop5" type="text" name="numberinop5" placeholder="No. of CBWTF in Operation">
            <b class="tooltip tooltip-bottom-right">CBWTF stands for Common Bio Medical Waste Treatment and Disposal Facility</b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberuc5" type="text" name="numberuc5" placeholder="No. of CBWTF Under Construction">
            <b class="tooltip tooltip-bottom-right">CBWTF stands for Common Bio Medical Waste Treatment and Disposal Facility</b>
        </label>
    </section>

</fieldset>

<fieldset>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberhcfcbwtf5" type="text" name="numberhcfcbwtf5" placeholder="No. of HCFs which are utilising CBWTF's">
            <b class="tooltip tooltip-bottom-right">CBWTF stands for Common Bio Medical Waste Treatment and Disposal Facility, HCF stand for Hospital and Nursing Homes </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberbmw5" type="text" name="numberbmw5" placeholder="Total quantity of BMW generated (kg/day)">
            <b class="tooltip tooltip-bottom-right">BMW stands for Bio Medical Waste</b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numbertbmw5" type="text" name="numbertbmw5" placeholder="Total quantity of BMW treated (kg/day)">
            <b class="tooltip tooltip-bottom-right">BMW stands for Bio Medical Waste</b>
        </label>
    </section>

</fieldset>

<fieldset>
    <section><label><strong>No. of Facilities violated BMW Rules</strong></label></section>
    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberviohcf5" type="text" name="numberviohcf5" placeholder="No. of Facilities violated BMW Rules For HCF's">
            <b class="tooltip tooltip-bottom-right">HCF stand for Hospital and Nursing Homes </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberviocbwtf5" type="text" name="numberviocbwtf5" placeholder="No. of Facilities violated BMW Rules For CBWTF's">
            <b class="tooltip tooltip-bottom-right">CBWTF stands for Common Bio Medical Waste Treatment and Disposal Facility</b>
        </label>
    </section>

</fieldset>


<fieldset>

    <section><label><strong>Total No. of Show Cause Notices/Directions issued to defaulter Facilities</strong></label></section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberschcf5" type="text" name="numberschcf5" placeholder="Total No. of Show Cause Notices/Directions issued to defaulter Facilities For HCF's">
            <b class="tooltip tooltip-bottom-right">HCF stand for Hospital and Nursing Homes </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numbersccbwtf5" type="text" name="numbersccbwtf5" placeholder="Total No. of Show Cause Notices/Directions issued to defaulter Facilities For CBWTF's">
            <b class="tooltip tooltip-bottom-right">CBWTF stands for Common Bio Medical Waste Treatment and Disposal Facility</b>
        </label>
    </section>

</fieldset>

<fieldset>
    <br><br><br>
</fieldset>

<!-------------------------------------- PART 5 Ends Here --------------------------------------------------->

<!-------------------------------------- PART 6 ------------------------------------------------------------->

<fieldset>
    <section>
        <label class="checkbox"><input type="checkbox" name="terms" id="a6" value="accept" checked><i></i>All others institutions generating bio-medical waste<em><span style="text-decoration: underline;">not included above</span></em></strong> <em>(uncheck if not applicable)</em></label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberhcf6" type="text" name="numberhcf6" placeholder="Total no. of  HCFs irrespective of no. of patients treated">
            <b class="tooltip tooltip-bottom-right">HCF stands for Hospitals & Nursing Homes </b>
        </label>
    </section>


    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberbeds6" type="text" name="numberbeds6" placeholder="Total no. of Beds">
            <b class="tooltip tooltip-bottom-right">Enter Total Number of Beds </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberra6" type="text" name="numberra6" placeholder="No. of HCF's required to take Authorisation">
            <b class="tooltip tooltip-bottom-right">HCF stands for Hospitals & Nursing Homes </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberga6" type="text" name="numberga6" placeholder="No. of HCf's Granted Authorisation">
            <b class="tooltip tooltip-bottom-right">HCF stands for Hospitals & Nursing Homes </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberotf6" type="text" name="numberotf6" placeholder="No. of HCF's having own treatment and disposal facilities">
            <b class="tooltip tooltip-bottom-right">HCF stands for Hospitals & Nursing Homes </b>
        </label>
    </section>
</fieldset>

<fieldset>
    <section><label><strong>Total no. of captive treatment equipment installed by the HCFs (i.e excluding Common Bio Medical Waste Treatment and Disposal Facility (CBMWTF))</strong></label></section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberapcd6" type="text" name="numberapcd6" placeholder="No. of Incinerators with APCD">
            <b class="tooltip tooltip-bottom-right">APCD stands for Air-Pollution control device </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberwapcd6" type="text" name="numberwapcd6" placeholder="No. of Incinerators without APCD">
            <b class="tooltip tooltip-bottom-right">APCD stands for Air-Pollution control device </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberaclave6" type="text" name="numberaclave6" placeholder="No. of Autoclave">
            <b class="tooltip tooltip-bottom-right">Enter Total Number of Autoclaves </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numbermw6" type="text" name="numbermw6" placeholder="No. of Microwave">
            <b class="tooltip tooltip-bottom-right">Enter Total Number of Microwave </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberhc6" type="text" name="numberhc6" placeholder="No. of Hydroclave">
            <b class="tooltip tooltip-bottom-right">Enter Total Number of Hydroclaves</b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numbersh6" type="text" name="numbersh6" placeholder="No. of Shredder">
            <b class="tooltip tooltip-bottom-right">Enter Total Number of Shredder</b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberdeep6" type="text" name="numberdeep6" placeholder="No. of Deep Burial">
            <b class="tooltip tooltip-bottom-right">Enter Total Number of Deep Burial</b>
        </label>
    </section>

</fieldset>

<fieldset>

    <section><label><strong>No. of CBWTF (Common Bio Medical Waste Treatment and Disposal Facility)</strong></label></section>

    <section>
        <label class="select">
            <select name="gender">
                <option value="0" selected disabled>Please specify if a hospital treatment facility is also used by other HCF</option>
                <option value="Yes">Yes</option>
                <option value="No">No</option>
                <b class="tooltip tooltip-bottom-right">Please select your Gender</b>
            </select>
            <i></i>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberinop6" type="text" name="numberinop6" placeholder="No. of CBWTF in Operation">
            <b class="tooltip tooltip-bottom-right">CBWTF stands for Common Bio Medical Waste Treatment and Disposal Facility</b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberuc6" type="text" name="numberuc6" placeholder="No. of CBWTF Under Construction">
            <b class="tooltip tooltip-bottom-right">CBWTF stands for Common Bio Medical Waste Treatment and Disposal Facility</b>
        </label>
    </section>

</fieldset>

<fieldset>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberhcfcbwtf6" type="text" name="numberhcfcbwtf6" placeholder="No. of HCFs which are utilising CBWTF's">
            <b class="tooltip tooltip-bottom-right">CBWTF stands for Common Bio Medical Waste Treatment and Disposal Facility, HCF stand for Hospital and Nursing Homes </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberbmw6" type="text" name="numberbmw6" placeholder="Total quantity of BMW generated (kg/day)">
            <b class="tooltip tooltip-bottom-right">BMW stands for Bio Medical Waste</b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numbertbmw6" type="text" name="numbertbmw6" placeholder="Total quantity of BMW treated (kg/day)">
            <b class="tooltip tooltip-bottom-right">BMW stands for Bio Medical Waste</b>
        </label>
    </section>

</fieldset>

<fieldset>
    <section><label><strong>No. of Facilities violated BMW Rules</strong></label></section>
    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberviohcf6" type="text" name="numberviohcf6" placeholder="No. of Facilities violated BMW Rules For HCF's">
            <b class="tooltip tooltip-bottom-right">HCF stand for Hospital and Nursing Homes </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberviocbwtf6" type="text" name="numberviocbwtf6" placeholder="No. of Facilities violated BMW Rules For CBWTF's">
            <b class="tooltip tooltip-bottom-right">CBWTF stands for Common Bio Medical Waste Treatment and Disposal Facility</b>
        </label>
    </section>

</fieldset>


<fieldset>

    <section><label><strong>Total No. of Show Cause Notices/Directions issued to defaulter Facilities</strong></label></section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numberschcf6" type="text" name="numberschcf6" placeholder="Total No. of Show Cause Notices/Directions issued to defaulter Facilities For HCF's">
            <b class="tooltip tooltip-bottom-right">HCF stand for Hospital and Nursing Homes </b>
        </label>
    </section>

    <section>
        <label class="input">
            <i class="icon-append fa fa-question"></i>
            <input id="numbersccbwtf6" type="text" name="numbersccbwtf6" placeholder="Total No. of Show Cause Notices/Directions issued to defaulter Facilities For CBWTF's">
            <b class="tooltip tooltip-bottom-right">CBWTF stands for Common Bio Medical Waste Treatment and Disposal Facility</b>
        </label>
    </section>

</fieldset>