<?php
define('VERSION', 'v1.0');
/**
 *
 *
 *
 */

class gatorconf {

	/**
	 *
	 * Configuration options
	 *
	 * You may need to logout/login for some changes to take effect
	 * 
	 * Make sure Apache service has read/write access to the repository folder(s)
	 * 
	 * NOTICE: Current .htaccess file inside repository directory will prevent script and html execution inside that directory
	 *
	 */
	public static function get($param) {

		$config = array(

		// users database and permissions (permissions: r - read; w - write; u - upload)
		// optionally you can specify homedir per user
		'users' => array(

		// PLEASE CHANGE THE ADMIN PASSWORD
		array('username' => 'admin', 'password' => 'admin123', 'permissions' => 'rw'),

		// sample user with all permissions
		// array('username' => 'john', 'password' => 'tomato', 'permissions' => 'rwu'),

		// sample user with read-only permission and home directory /var/repos
		// array('username' => 'mike', 'password' => 'potato', 'permissions' => 'r', 'homedir' => '/var/repos'),

		// do not remove this account, you can disable guests bellow
		array('username' => 'guest', 'password' => '', 'permissions' => 'r'),

		), // users array end

		// allow guest account (true/false)
		'allow_guests' => false,

		// main file repository
		// this is also a repository for users without specific homedir
		// you'll need to make sure your webserver can write here
		'repository' => getcwd().'/filestorage',

		// allow clickable links on files (true/false)
		// set to false if repository is not inside server's public directory
		'allow_file_links' => true,

		// use lightbox plugin to preview images (true/false)
		// this also enables allow_file_links
		'use_lightbox_gallery' => true,

		// accepted file types when uploading (set to * if you want no restrictions)
		'accept_file_extensions' => array('gif','jpg','jpeg','png','xls'),

		// max number of files on batch upload
		'max_files' => 25,

		// maximum file size in bytes when uploading
		// The php.ini settings upload_max_filesize and post_max_size
		// take precedence over the following setting
		'max_filesize' => 2097152, // 2MB

		// time/date format - see php date()
		'time_format' => 'd/m/y h:i:s',

		// use simple copy-move instead of cut-copy-paste (true/false)
		'simple_copy_move' => true,
		
		// use zip functions (true/false)
		// zip extension must be enabled on server (see http://php.net/manual/en/book.zip.php)
		'use_zip' => true,

		// files can be edited (true/false)
		'allow_edit_files' => true,

		// files/folders can be renamed (true/false)
		'allow_rename_files' => true,

		// show top-bar (true/false)
		'show_top_auth_bar' => true,

		// this restricted files will be hidden (no wildcards)
		'restricted_files' => array('.htaccess'),

		// encrypt filenames for url actions (true/false)
		'encrypt_filenames' => true,

		// encryption salt, please change this to something random
		'encryption_salt' => 'RandomStringABC1234567890',

		// clear url after chdir (true/false)
		// this will disable browser's back button but the url will be cleaner
		'flush_url_on_chdir' => false,
				
		// mode when creating new directory (ignored on windows)
		'new_dir_mode' => 0755,

		// use authentication module (true/false)
		// WARNING: if you set this to false anyone can see, change or delete your files without need to login
		// this will also disable encrypt_filenames
		'use_auth' => true,
		
		// server url and base bath, usually you don't need to change this
		'base_url' => 'http://'.$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']),
		'base_path' => getcwd(),
		
		// write user actions to usage.log file (true/false)
		'write_log' => false,

		);

		/**
		 *
		 * End of configuration options
		 *
		 */



		$config['base_path'] = str_replace('\\', '/', $config['base_path']);
		if ($param != 'base_path') $config = gator::validateConf($config, $param);
		return $config[$param];
	}
}
require_once gatorconf::get('base_path')."/include/file-gator.php";
new gator();
