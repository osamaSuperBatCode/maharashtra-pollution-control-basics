<body class="main">
<!--<script language="javascript" type="text/javascript"-->
<!--        src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>-->

<script language="javascript" type="text/javascript">
    $(document).ready(function() {
        $('#search').keyup(function() {
            searchTable($(this).val());
        });
    });
    function searchTable(inputVal) {
        var table = $('#searchTable');
        table.find('tr').each(function(index, row) {
            var allCells = $(row).find('td');
            if (allCells.length > 0) {
                var found = false;
                allCells.each(function(index, td) {
                    var regExp = new RegExp(inputVal, 'i');
                    if (regExp.test($(td).text())) {
                        found = true;
                        return false;
                    }
                });
                if (found == true)
                    $(row).show();
                else
                    $(row).hide();
            }
        });
    }
</script>


<!-- prepare upload templates -->
<?php gator::display("main_tmpl.php")?>

<?php if(gatorconf::get('use_auth') == true && gatorconf::get('show_top_auth_bar') == true && $_SESSION['simple_auth']['username'] != 'guest'):?>
<div class="top-menu">
<div class="row">
<a class="version-info">MPC Panel</a>

 <?php echo $_SESSION['simple_auth']['username']?>
 | <a href="?logout=1">Sign out</a>
</div>
</div>
<div class="top-menu-spacer"></div>
<?php endif;?>

<div id="wrapper" class="row">
<div class="container twelve columns">
<div id="topcorners"></div>
<div id="content">

<?php if(gatorconf::get('use_auth') == true && gatorconf::get('show_top_auth_bar') == false && $_SESSION['simple_auth']['username'] != 'guest'):?>
<div class="small-auth-menu">
 <a class="version-info">MPC Panel</a>&nbsp;
 | <?php echo $_SESSION['simple_auth']['username']?>
 | <a href="?logout=1">Sign out</a>
</div>
<?php endif;?>

<?php if(gatorconf::get('use_auth') == true && $_SESSION['simple_auth']['username'] == 'guest'):?>
<div class="small-auth-menu">
 <a href="?login=1">Sign in</a>
</div>
<?php endif;?>

<div id="logo">
<!--<a href="--><?php //echo gatorconf::get('base_url')?><!--?cd="><img alt="filegator" src="--><?php //echo gatorconf::get('base_url')?><!--/include/views/img/logo.gif"></a>-->
</div>

<div class="fileupload-container navigation-button">
    <!-- The file upload form used as target for the file upload widget -->
    <form id="fileupload" action="#" method="POST" enctype="multipart/form-data">
        <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
        <div class="nav fileupload-buttonbar">
                
					
				<?php if (gator::checkPermissions('ru')):?>
                <!-- The fileinput-button span is used to style the file input field as button -->
                <span class="fileinput-button nice radius button">
                    <i class="icon-plus icon-white"></i>
					
					<span class="">Add Files...</span>

                    <input type="file" name="files[]" multiple>
                    <input type="hidden" name="uniqid" value="50338402749c1">
                </span>
                <?php endif;?>
                
                <div class="clear"></div>
                
        </div>
        
        <!-- The table listing the files available for upload/download -->
		<div id="top-panel">
        <table role="presentation" class="table table-striped">
         <tbody class="files" data-toggle="modal-gallery" data-target="#modal-gallery">
         </tbody>
        </table>
        
        </div>
    </form>
</div>

<?php if (gator::checkPermissions('rw')):?>
    <?php if (gator::checkPermissions('rw')):?>
<button type="button" class="nice radius button select-button">Select All</button>

<?php if (gatorconf::get('simple_copy_move')):?>
        <button type="button" class="nice secondary radius button simple-copy-selected">Copy</button>
        <button type="button" class="nice secondary radius button simple-move-selected">Move</button>
    <?php else:?>
<button type="button" class="nice secondary radius button cut-selected">Cut</button>
<button type="button" class="nice secondary radius button copy-selected">Copy</button>
<button type="button" class="nice secondary radius button paste-selected"<?php if (!isset($_SESSION['buffer'])) echo ' disabled="disabled"'?>">Paste</button>
<?php endif;?>

        <?php if (gatorconf::get('use_zip')):?>
            <button type="button" class="nice secondary radius button zip-selected">Zip</button>
        <?php endif;?>
<button type="button" class="nice secondary radius button delete-selected">Delete</button>
    <div class="search"><span class="search-img"></span><input type="search" id="search" class="inputtext" placeholder="Search and Filter" />
<?php endif;?>
<div class="nice radius button split dropdown right">
  <a class="sort-invert">Sort by <?php echo ucfirst($_SESSION['sort']['by']);?></a>
  <span></span>
  <ul>
    <li><a class="sort-by-name">Sort by Name</a></li>
    <li><a class="sort-by-date">Sort by Date</a></li>
    <li><a class="sort-by-size">Sort by Size</a></li>
  </ul>
</div>

</div>
<br><br>
<div id="newfolder_button" class="navigation-button-right">
<!-- <input type="text" class="inputtext" name="newfolder" id="newfolder">-->

<!-- <div class="nice radius button split dropdown navigation-button-right">-->
<!--  <a class="new-folder">New Folder</a>-->
<!--  <span></span>-->
<!--  <ul>-->
<!--	<li><a class="new-folder">Create New Folder</a></li>-->
<!--    <li><a class="new-file">Create New File</a></li>-->
<!--  </ul>-->
<!--</div>-->

</div>


<?php endif;?>
                
<div id="close-top-panel" class="clear">
<button type="button" class="nice radius button right upload-done">Done</button>
</div>

<div id="browse-panel">

<div class="breadcrumbs">
<span>
<?php foreach($params['breadcrumb'] as $key => $value):?>
<?php if($key != 'Home') echo '&raquo;&nbsp;'?>
<a href="<?php echo $value?>"><?php echo $key?></a>
<?php endforeach;?>
</span>
</div>
<a class="directory-tree"></a>
<div class="clear"></div>

<form id="fileset" action="?" method="POST" accept-charset="UTF-8">

<?php gator::display("main_filelist.php", $params)?>

<div class="bottom-actions">

</form>
</div> <!-- end browse-panel -->
</div>
<div id="bottomcorners"></div>
</div>
</div>

<div id="modal" class="reveal-modal"></div>
<div id="second_modal" class="reveal-modal"></div>
<div id="big_modal" class="reveal-modal large"></div>

<?php gator::display("main_js.php")?>