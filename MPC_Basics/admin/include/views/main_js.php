<script>
//<![CDATA[
$(document).ready(function() {

    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload();
    	
    $('#fileupload').fileupload('option', {
        url: '<?php echo gatorconf::get('base_url')?>/?upload=1',
        maxFileSize: <?php echo gatorconf::get('max_filesize')?>,
        maxNumberOfFiles: <?php echo gatorconf::get('max_files')?>,
        downloadTemplateId: false,
		autoUpload: true,
        acceptFileTypes: <?php echo gatorconf::get('accept_file_extensions')?>
    });

 	// clear checkboxes
	$('td input[type="checkbox"]').each( function() {$(this).attr("checked", null);});

    $(document).foundationButtons();

 	// Hover shim for Internet Explorer 6 and Internet Explorer 7.
    $(document).on('hover','a',function(){
        $(this).toggleClass('hover');
    });


 	// filegator info
    $('a.version-info').click(function(){
        
    	$('#modal').html(' ');

 		var output = '<div class="modal-content"><h5>MPC Admin Panel <?php echo VERSION?></h5><hr />';
 		output += '<p></p><p>Insert Instructions Here.</p><p>';

 		output += '<hr /></div>';

 		output += '<div class="modal-buttons right">';
 		output += '</div>';
 		
 		output += '<a class="close-reveal-modal"></a>';
 		
 		$('#modal').append(output);
 		$('#modal').reveal();

 		$('#cancel-button').click(function(){
 			$('#second_modal').trigger('reveal:close');
 	    });
    });
    
       	    
    // new folder
    $('a.new-folder').click(function(){
    	var name = $('input#newfolder').val();
    	window.location.href = '<?php echo gatorconf::get('base_url')?>?newdir='+name;

    });

    // new file
    $('a.new-file').click(function(){
    	var name = $('input#newfolder').val();
    	window.location.href = '<?php echo gatorconf::get('base_url')?>?newfile='+name;

    });

    // submit new folder on enter key
    $('input#newfolder').keypress(function(e){
	    	if(e.keyCode == 13){
	    		var name = $('input#newfolder').val();
	        	window.location.href = '<?php echo gatorconf::get('base_url')?>?newdir='+name;
	   		 return false;
		   	 }else{
		   		 return true;
		   	 }
    });

    // show tree
    $('.directory-tree').click(function(){
    	showDirectoryTree('Folder Structure', '/?tree=cd');
    });
    
    // toggle selection
    $('.select-button').click(function(){
    	if ($('button.select-button').html() == 'Select None'){
    		$('td input[type="checkbox"]').each( function() {$(this).attr("checked", null);});
    		$('button.select-button').html('Select All');
    	
    	}else{
    		$('td input[type="checkbox"]').each( function() {$(this).attr("checked", status);});
    		$('button.select-button').html('Select None');
    	}
    });

    // finish with upload
    $('.upload-done').click(function(){

        var progress = $('.uploading');

        if ($(progress).size()!=0){

        	$('#modal').html(' ');

    		var output = '<div class="modal-content"><h3>Upload in progress...</h3><hr /><h5>Do you want to cancel upload?</h5></div>';

    		output += '<div class="modal-buttons right">';
    		output += '<button id="stop-button" type="button" class="nice radius button">Stop Upload</button>';
    		output += '</div>';
    		
    		output += '<a class="close-reveal-modal"></a>';
    		
    		$('#modal').append(output);
    		$('#modal').reveal();

    		$('#stop-button').click(function(){
    			window.location.href = '<?php echo gatorconf::get('base_url')?>';
    	    });

        	return;
        }
        
    	window.location.href = '<?php echo gatorconf::get('base_url')?>';
    });

    // cut / copy / paste
    $('.cut-selected').click(function(){
    	submitAction('cut', true);
    });
    $('.copy-selected').click(function(){
    	submitAction('copy', true);
    });
    $('.paste-selected').click(function(){

    	submitAction('paste', false);

       	$('#modal').html('<p class="lead">Please wait...</p>');
       	$('#modal').reveal({
        	     animation: 'none', //fade, fadeAndPop, none
        	     animationspeed: 0 //how fast animations are
       	});
       	$('body *').unbind();


    });
    $('.simple-copy-selected').click(function(){
    	// notice if nothing is selected
    	if (isSelected() == false) return false;
    	
    	showDirectoryTree('Select Destination Folder', '/?tree=copy', 'simple-copy');
    });
    $('.simple-move-selected').click(function(){
    	// notice if nothing is selected
    	if (isSelected() == false) return false;
    	
    	showDirectoryTree('Select Destination Folder', '/?tree=move', 'simple-move');
    });

    // zip selected
    $('.zip-selected').click(function(){
        
    	// notice if nothing is selected
    	if (isSelected() == false) return false;
    	
    	$('#modal').html(' ');

		var output = '<div class="modal-content"><h5>Add to archive:</h5><input type="text" name="archive-name" id="archive-name" value="archive.zip" class="text ui-widget-content ui-corner-all" /></div>';

		output += '<div class="modal-buttons right">';
		output += '<button id="confirm-button" type="button" class="nice radius button">Create Zip</button>';
		output += '<button id="cancel-button" type="button" class="nice radius button">Cancel</button>';
		output += '</div>';
		
		output += '<a class="close-reveal-modal"></a>';
		
		$('#modal').append(output);
		$('#modal').reveal();

		$('#cancel-button').click(function(){
			$('#modal').trigger('reveal:close');
	    });
	    
		$('#confirm-button').click(function(){

			var archive_name = $('#archive-name').val();

    		if (archive_name == ''){
    			$('#modal').trigger('reveal:close');
        		 return;
    		}
    		
			$('<input>').attr({
			    type: 'hidden',
			    name: 'archivename',
			    value: archive_name
			})
			.appendTo('form#fileset');

			$('<input>')
			.attr({
			    type: 'hidden',
			    name: 'action',
			    value: 'zip'
			})
			.appendTo('form#fileset');
			
	    	$('#modal').html('<p class="lead">Please wait...</p>');
	    	$('form#fileset').submit();
			$('body *').unbind();
	    });

    });

    // delete selected
    $('.delete-selected').click(function(){

    	// notice if nothing is selected
    	if (isSelected() == false) return false;

    	$('#modal').html(' ');
    	var output = '<p class="lead">Are you sure you want to delete selected items?</p><hr />';
    	output += '<div class="modal-buttons right">';
    	output += '<button id="confirm-button" type="button" class="nice alert radius button">Delete</button>';
    	output += '<button id="cancel-button" type="button" class="nice radius button">Cancel</button>';
    	output += '</div>';
    	output += '<a class="close-reveal-modal"></a>';

    	$('#modal').append(output);
    	$('#modal').reveal();
    	
	    $('#cancel-button').click(function(){
	    	$('#modal').trigger('reveal:close');
	    });

	    $('#confirm-button').click(function(){

	    	$('<input>').attr({
			    type: 'hidden',
			    name: 'action',
				value: 'delete'
			}).appendTo('form#fileset');

	    	$('#modal').html('<p class="lead">Please wait...</p>');	
	    			
			$('form#fileset').submit();
			$('body *').unbind();
	    });

	    return;
    });

    // sorting
    $('.sort-invert').click(function(){
    	window.location.href = '<?php echo gatorconf::get('base_url')?>?sortinvert';	
    });
    $('.sort-by-name').click(function(){
    	window.location.href = '<?php echo gatorconf::get('base_url')?>?sortby=name';	
    });
    $('.sort-by-date').click(function(){
    	window.location.href = '<?php echo gatorconf::get('base_url')?>?sortby=date';	
    });
    $('.sort-by-size').click(function(){
    	window.location.href = '<?php echo gatorconf::get('base_url')?>?sortby=size';	
    });


    // single file settings - buttons & actions
    $('.action-info').click(function(){
    	var permissions = '<?php echo $_SESSION['simple_auth']['permissions']?>';
    	var allow_links = '<?php echo gatorconf::get('allow_file_links')?>';

    	var data_type = $(this).attr('data-type');
    	var data_link = $(this).attr('data-link');
		var data_name = $(this).attr('data-name');
		var data_crypt = $(this).attr('data-crypt');
		var data_size = $(this).attr('data-size');
		var data_time = $(this).attr('data-time');
    	
    	$('#modal').html(' ');

   		var output = '<div class="modal-descr"><h4>'+data_name+'</h4><hr />';
    
    	// if links are not disabled and not dir
    	if (allow_links != '' && data_type != 'dir'){
    		output += '<div class="modal-content"><h5>Download Link:</h5><input type="text" name="link" id="link" value="'+data_link+'" class="text ui-widget-content ui-corner-all" readonly="readonly" /></div>';
    	}

    	output += '<div class="modal-buttons right">';

    	// download sub-button
    	if (data_type != 'dir'){
    		output += '<button id="download-button" type="button" class="nice radius button">Download</button>';
    	}
    	
    	// rename & unzip sub-buttons - if we have write permissions
    	if (permissions.indexOf("w") != -1){

    		<?php if (gatorconf::get('use_zip')):?>
        	if (data_type == 'zip'){
        		output += '<button id="unzip-button" type="button" class="nice radius button">Unzip</button>';	
        	}
        	<?php endif;?>

        	<?php if (gatorconf::get('allow_edit_files')):?>
        	if (data_type == 'generic'){
        		output += '<button id="edit-button" type="button" class="nice radius button">Edit</button>';	
        	}
        	<?php endif;?>

        	<?php if (gatorconf::get('allow_rename_files')):?>
    		output += '<button id="rename-button" type="button" class="nice radius button">Rename</button>';
    		<?php endif;?>	
    	}
    
    	// cancel sub-button
    	output += '<button id="cancel-button" type="button" class="nice radius button">Close</button>';
    	output += '</div>';
    	output += '<a class="close-reveal-modal"></a>';
    	
    	$('#modal').append(output);
    	$('#modal').reveal();

    	$('#cancel-button').click(function(){
        	$('#modal').trigger('reveal:close');
        });

    	$('#download-button').click(function(){
    		window.location.href = '<?php echo gatorconf::get('base_url')?>?download='+data_crypt;
        });

    	$('#unzip-button').click(function(){

    		$('#second_modal').html(' ');

    		var output = '<div class="modal-content"><h5>Unzip archive content here?</h5></div><hr />';

    		output += '<div class="modal-buttons right">';
    		output += '<button id="confirm-button" type="button" class="nice radius button">Unzip</button>';
    		output += '<button id="second-cancel-button" type="button" class="nice radius button">Cancel</button>';
    		output += '</div>';
    		
    		output += '<a class="close-reveal-modal"></a>';
    		
    		$('#second_modal').append(output);
    		$('#second_modal').reveal();

    		$('#second-cancel-button').click(function(){
    			$('#second_modal').trigger('reveal:close');
    	    });
    	    
    		$('#confirm-button').click(function(){
    			$('<input>').attr({
    			    type: 'hidden',
    			    name: 'filename',
    			    value: data_crypt
    			})
    			.appendTo('form#fileset');

    			$('<input>')
    			.attr({
    			    type: 'hidden',
    			    name: 'action',
    			    value: 'unzip'
    			})
    			.appendTo('form#fileset');
    			
    	    	$('#second_modal').html('<p class="lead">Please wait...</p>');
    			$('form#fileset').submit();
    			$('body *').unbind();
    	    });
    		
        });

    	$('#rename-button').click(function(){

    		$('#second_modal').html(' ');

    		if (data_type == 'dir'){
    			var itemt = 'Folder';
    		}else{
        		var itemt = 'File';
    		}
    		
    		var output = '<div class="modal-content"><h5>Rename '+itemt+':</h5><input type="text" name="new-name" id="new-name" value="'+data_name+'" crypt="'+data_crypt+'" class="text ui-widget-content ui-corner-all" /></div>';

    		output += '<div class="modal-buttons right">';
    		output += '<button id="confirm-button" type="button" class="nice radius button">Rename</button>';
    		output += '<button id="second-cancel-button" type="button" class="nice radius button">Cancel</button>';
    		output += '</div>';
    		
    		output += '<a class="close-reveal-modal"></a>';
    		
    		$('#second_modal').append(output);
    		$('#second_modal').reveal();

    		$('#second-cancel-button').click(function(){
    			$('#second_modal').trigger('reveal:close');
    	    });

    		$('#confirm-button').click(function(){

    			var newname = $('#new-name').val();

        		if (newname == ''){
        			$('#second_modal').trigger('reveal:close');
            		 return;
        		}
    			
    			$('<input>')
    			.attr({
    			    type: 'hidden',
    			    name: 'oldname',
    			    value: data_crypt					    
    			})
    			.appendTo('form#fileset');
    			
    			$('<input>').attr({
    			    type: 'hidden',
    			    name: 'newname',
    			    value: newname
    			})
    			.appendTo('form#fileset');

    			$('<input>')
    			.attr({
    			    type: 'hidden',
    			    name: 'action',
    			    value: 'rename'
    			})
    			.appendTo('form#fileset');

    			$('form#fileset').submit();
    	    });
        });

    	$('#edit-button').click(function(){
        	
    		$('#big_modal').html(' ');

    		var output = '<textarea id="file-content" cols="50" rows="25"></textarea>';

    		output += '<div class="modal-buttons right">';
    		output += '<button id="save-button" type="button" class="nice radius button">Save</button>';
    		output += '<button id="second-cancel-button" type="button" class="nice radius button">Cancel</button>';
    		output += '</div>';
    		
    		output += '<a class="close-reveal-modal"></a>';
    		
    		$('#big_modal').append(output);
    		
    		$.get('<?php echo gatorconf::get('base_url')?>/?edit-load='+data_crypt, function(data) {
    			  $('textarea#file-content').val(data);
    			});
			
    		$('#big_modal').reveal({ closeOnBackgroundClick: false});

    		$('#second-cancel-button').click(function(){
    			$('#big_modal').trigger('reveal:close');
    	    });

    		$('#save-button').click(function(){
    			var content = $('textarea#file-content').val();
    		
    			$('<input>').attr({
    			    type: 'hidden',
    			    name: 'filename',
    			    value: data_crypt
    			})
    			.appendTo('form#fileset');
    			
    			$('<input>').attr({
    			    type: 'hidden',
    			    name: 'content',
    			    value: content
    			})
    			.appendTo('form#fileset');

    			$('<input>')
    			.attr({
    			    type: 'hidden',
    			    name: 'action',
    			    value: 'edit-save'
    			})
    			.appendTo('form#fileset');

    			$('form#fileset').submit();
    		});
        });

    	return;
    });
  
});
function isSelected(){
	if ($('td input[type="checkbox"]:checked').is(":empty") == false){
		
		$('#modal').html(' ');
		$('#modal').append('<p class="lead">Use checkboxes to select items.</p><hr />');
		$('#modal').append('<button id="cancel-button" type="button" class="nice radius button right">Close</button>');
		$('#modal').append('<a class="close-reveal-modal"></a>');
		$('#modal').reveal();
		
	    $('#cancel-button').click(function(){
	    	$('#modal').trigger('reveal:close');
	    });

		return false;
	}
	return true;
}
function submitAction(action, checkSelection, destination){
	// notice if nothing is selected
	if (checkSelection == true && isSelected() == false) return false;

	if (destination){
		$('<input>').attr({
		    type: 'hidden',
		    name: 'destination',
			value: destination
		}).appendTo('form#fileset');
	}
	
	$('<input>').attr({
	    type: 'hidden',
	    name: 'action',
		value: action
	}).appendTo('form#fileset');
	
	$('form#fileset').submit();
}
function showDirectoryTree(title, ajaxcall, post_action){
	
	$('#big_modal').html(' ');

	var output = '<p class="lead text-search-info">'+title+'</p>';
	output += '<hr />';
	
	output += '<p id="dir-links" class="lead">Please wait...</p>';
	output += '<p id="dir-links-backup" style="display:none"></p>';
	
	output += '<hr /><div class="text-search-box"><span class="search-img"></span><input type="text" id="text-search" class="inputtext" placeholder="Search and Filter" />';
	output += '</div>';

	output += '<a class="close-reveal-modal"></a>';
	
	$('#big_modal').append(output);
	$('#big_modal').reveal();

	// get data via ajax
	$.get('<?php echo gatorconf::get('base_url')?>'+ajaxcall, function(data) {
			$('#dir-links').html(data);
			$('#dir-links-backup').html(data);

			bindTreeLinks(post_action);
	});

	// case-insensitive search extension (jQuery 1.8+)
	jQuery.expr[':'].Contains = jQuery.expr.createPseudo(function(arg) {
      return function( elem ) {
	    return ( elem.textContent || elem.innerText || getText( elem ) ).toUpperCase().indexOf(arg.toUpperCase()) >= 0;
	      };
    });
		
	// search and filter
	$('#text-search').bind('keyup change', function() {

    	// pull in the new value
        var searchTerm = $('#text-search').val();

        // reset from backup
        var backup =  $('#dir-links-backup').html();
		$('#dir-links').html(backup);

        // disable filter if empty
        if (searchTerm) {

			var match = $("#dir-links li:Contains('"+searchTerm+"')");

			// remove unwanted
			$(match).each(function(){
				$(this).find("li:not(:Contains('"+searchTerm+"'))").remove();
			});

			// remove double
			match = $(match).first();
			
			if ($(match).length == 0){
				$('#dir-links').html('<p>Nothing found...</p>');
				return;
			}

			// load the whole thing
			var html = $('<div>').append($(match).clone()).remove().html();
			$('#dir-links').html(html);
        }
        
        bindTreeLinks(post_action);
    });
  
}
function bindTreeLinks(post_action){
	$('#dir-links li[clink]').click(function(e){
		// stop overlapping li
		e.stopPropagation();
		
		var link = $(this).attr('clink');

		// this is post action / form submit?
		if (post_action){
			submitAction(post_action, false, link);

	    	$('#big_modal').html('<p class="lead">Please wait...</p>');
	       	$('body *').unbind();
	       	
			return;
		}

		// this is get, change dir
		window.location.href = '<?php echo gatorconf::get('base_url')?>'+link;

		return;

    });
}
//]]>
</script>