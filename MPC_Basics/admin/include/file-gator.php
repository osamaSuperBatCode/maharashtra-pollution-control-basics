<?php
define('DS', '/');

class gator {

	/**
	 *
	 * main init
	 */
	public function __construct() {
			
		// start session & handle logout
		session_start();
		if(isset($_GET["logout"]) && $_GET["logout"] == 1){
			gator::writeLog('logout');

			session_destroy();
			session_start();
		}

		// test if file-upload ajax call
		if (isset($_GET['upload']) && $_GET['upload'] == '1' && gator::checkPermissions('ru')){
			$this->ajaxUpload();
		}

		// use auth?
		if (gatorconf::get('use_auth') == true){
			$this->authenticate();
		}else{
			$_SESSION['simple_auth']['permissions'] = 'rwu';
			$_SESSION['simple_auth']['username'] = 'guest';
			$_SESSION['simple_auth']['cryptsalt'] = gatorconf::get('encryption_salt');
		}

		// handle actions (copy, paste, delete, rename etc.)
		$this->actions();

		$this->changeDirectory();

		// get current directory file-list
		$listing = gator::getDirectoryList();

		gator::display("header.php");
		gator::display("main.php", $listing);
		gator::display("footer.php");

	}


	/**
	 * 
	 * get and post actions
	 * s
	 */
	public function actions(){

		// post actions
		if (isset($_POST['action']) && gator::checkPermissions('rw')){
		
			$action = $_POST['action'];
			unset($_POST['action']);

			switch ($action){

				case 'delete':

					foreach ($_POST as $post_file){

						if (in_array($post_file, gatorconf::get('restricted_files'))) continue;

						$files[] = $this->filterInput($this->decrypt($post_file));
					}

					$this->deleteFiles($files, $_SESSION['cwd']);

					break;

				case 'rename':

					if (gatorconf::get('allow_rename_files') == false || !isset($_POST['oldname']) || !isset($_POST['newname'])) break;

					$oldname = $this->filterInput($this->decrypt($_POST['oldname']));
					$newname = $this->filterInput($_POST['newname']);

					if (in_array($oldname, gatorconf::get('restricted_files')) || in_array($newname, gatorconf::get('restricted_files'))) break;

					$this->renameFile($oldname, $newname);

					break;

				case 'edit-save':

					if (gatorconf::get('allow_edit_files') == false || !isset($_POST['filename'])) break;

					$filename = $this->filterInput($this->decrypt($_POST['filename']));
					$content = $_POST['content'];

					if (in_array($filename, gatorconf::get('restricted_files'))) break;

					file_put_contents($_SESSION['cwd'].DS.$filename, $content);

					gator::writeLog('edit file / save - '.$filename);

					break;

				case 'zip':

					if (!isset($_POST['archivename'])) break;
					$archive_name = $this->filterInput($_POST['archivename']);
					unset($_POST['archivename']);

					foreach ($_POST as $post_file){
						$files[] = $this->filterInput($this->decrypt($post_file));
					}

					$this->zipFiles($files, $archive_name);

					break;

				case 'unzip':

					if (!isset($_POST['filename'])) break;

					$filename = $this->filterInput($this->decrypt($_POST['filename']));

					$this->unzipFile($filename);

					break;

				case 'copy':
						
					foreach ($_POST as $post_file){
						$files[] = $this->filterInput($this->decrypt($post_file));
					}
					$this->pushToBuffer($files, 'copy');

					break;

				case 'cut':

					foreach ($_POST as $post_file){
						$files[] = $this->filterInput($this->decrypt($post_file));
					}
					$this->pushToBuffer($files, 'cut');

					break;

				case 'paste':

					$this->pasteFromBuffer();

					break;

				case 'simple-copy':
				case 'simple-move':

					// link to home dir is blank
					if (!isset($_POST['destination'])) $_POST['destination'] = '';

					$destination = $this->filterInput($this->decrypt($_POST['destination']), false);
					unset($_POST['destination']);

					foreach ($_POST as $post_file){
						$files[] = $this->filterInput($this->decrypt($post_file));
					}

					if ($action == 'simple-copy'){
						$this->copyFiles($files, $_SESSION['cwd'], gatorconf::get('repository').DS.$destination);
					}

					if($action == 'simple-move'){
						$this->moveFiles($files, $_SESSION['cwd'], gatorconf::get('repository').DS.$destination);
					}

					break;

				default:
					break;
			}

			// flush url
			header('Location: '.gatorconf::get('base_url'));
			die;
		}

		// download file
		if (isset($_GET['download']) && !empty($_GET['download'])){

			$filename = $this->filterInput($this->decrypt($_GET['download']));

			if (in_array($filename, gatorconf::get('restricted_files'))) die;

			if (!file_exists($_SESSION['cwd'].DS.$filename)) die;
				
			// Set headers
			header("Cache-Control: public");
			header("Content-Description: File Transfer");
			header("Content-Disposition: attachment; filename=$filename");
			header("Content-Type: application/octet-stream");
			header("Content-Transfer-Encoding: binary");
				
			// output file
			readfile($_SESSION['cwd'].DS.$filename);

			gator::writeLog('download - '.$filename);

			die;
		}

		// edit action - load file content via this ajax
		if (isset($_GET['edit-load']) && gator::checkPermissions('rw') && gatorconf::get('allow_edit_files') == true){

			$filename = $this->filterInput($this->decrypt($_GET['edit-load']));

			if (in_array($filename, gatorconf::get('restricted_files'))) die;

			if (!file_exists($_SESSION['cwd'].DS.$filename)) die;

			echo file_get_contents($_SESSION['cwd'].DS.$filename);

			gator::writeLog('edit file / load - '.$filename);

			die;
		}

		// new folder / new file
		if ((isset($_GET['newdir']) || isset($_GET['newfile'])) && gator::checkPermissions('rw')){

			if (isset($_GET['newdir'])){
				$newdir = $this->filterInput($_GET['newdir']);
				
				if (!in_array($newdir, gatorconf::get('restricted_files')))
				mkdir($_SESSION['cwd'].DS.$newdir, gatorconf::get('new_dir_mode'));
				
			} elseif (isset($_GET['newfile'])){
				$newfile = $this->filterInput($_GET['newfile']);
				
				if (!in_array($newfile, gatorconf::get('restricted_files')))
				touch($_SESSION['cwd'].DS.$newfile);
			}

			gator::writeLog('create new - '.$newdir.$newfile);

			// flush url
			header('Location: '.gatorconf::get('base_url'));
			die;
		}

		// sorting
		if (isset($_GET['sortby']) || isset($_GET['sortinvert'])){

			if (isset($_GET['sortby'])){
				$_SESSION['sort']['by'] = $this->filterInput($_GET['sortby']);
				$_SESSION['sort']['order'] = 1;
			} elseif (isset($_GET['sortinvert'])){
				$_SESSION['sort']['order'] *= -1;
			}

			gator::writeLog('sort order '.$_SESSION['sort']['by']);

			// flush url
			header('Location: '.gatorconf::get('base_url'));
			die;

		}elseif(!isset($_SESSION['sort']['by'])){
			$_SESSION['sort']['by'] = 'name';
			$_SESSION['sort']['order'] = 1;
		}

		// directory tree - ajax load
		if (isset($_GET['tree']) || !empty($_GET['tree'])){

			$tree_action = $this->filterInput($_GET['tree']);

			$dirs = '';

			if ($tree_action == 'cd'){
				$dirs = $this->getDirectoryTree(gatorconf::get('repository'), false, '?cd=');
			}

			if ($tree_action == 'copy' || $tree_action == 'move'){
				$dirs = $this->getDirectoryTree(gatorconf::get('repository'), true, '');
			}

			echo $dirs;

			gator::writeLog('tree load');
			die;
		}

		return;
	}


	/**
	 *
	 * set current working directory
	 */
	public function changeDirectory(){

		// in no session or home - set defaults
		if (!isset($_SESSION['cwd']) || (isset($_GET['cd']) && $this->decrypt($_GET['cd']) == '')){
			$_SESSION['cwd'] = gatorconf::get('repository');

			gator::writeLog('change dir - home');

			return;
		}

		// get directory from url
		$input = (isset($_GET['cd']) ? $this->filterInput($this->decrypt($_GET['cd']), false) : false);

		if ($input && strpos($input, '..') === false){

			$childDir = gatorconf::get('repository').DS.$input;
			$childDir = str_replace('\\', '/', $childDir);

			// do not allow chdir outside reposiroty
			if (strstr($childDir, gatorconf::get('repository')) == false){
				header('Location: '.gatorconf::get('base_url'));
				die;
			}

			if (is_dir($childDir)){
				// change to dir if exists
				$_SESSION['cwd'] = $childDir;

				gator::writeLog('change dir - '.$input);
			}

			// reload page
			if (gatorconf::get('flush_url_on_chdir')){
				header('Location: '.gatorconf::get('base_url'));
				die;
			}
		}

		return;
	}


	/**
	 *
	 * fix path to work with url
	 */
	public static function encodeurl($string){

		$string = rawurlencode($string);

		// do not encode /, :
		$string = str_replace("%2F", "/", $string);
		$string = str_replace("%5C", "/", $string);

		$string = str_replace("%3A", ":", $string);

		return $string;
	}


	/**
	 *
	 * filter user's input
	 */
	public function filterInput($string, $strict = true){

		// replace multiple spaces and newlines
		$string = preg_replace('/\s\s+/', ' ', $string);
		
		// bad chars
		$strip = array("..", "`", "*", "[", "{", "]", "}", "|", ";", ":", "\"", "'", 
                   "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;","â€”", "â€“", 
                   ",", "<", ">", "?");
		
		// we need this sometimes
		if ($strict) array_push($strip, "/", "\\");
		
		$clean = trim(str_replace($strip, "_", strip_tags($string)));

		return $clean;
	}


	/**
	 *
	 * display views controller
	 */
	public static function display($view, $params = null){

		require_once gatorconf::get('base_path')."/include/views/".$view;
	}


	/**
	 *
	 * Delete file or dir
	 */
	public function deleteFiles($files, $directory){

		foreach ($files as $file){

			gator::writeLog('delete - '.$file);

			if ($file == '.' || $file == '..') continue;

			if (is_dir($directory.DS.$file) == true){
				$this->recursiveRemoveDirectory($_SESSION['cwd'].DS.$file);
				$this->syncBufferFile($_SESSION['cwd'].DS.$file);
				continue;
			}

			unlink($directory.DS.$file);

			$this->syncBufferFile($directory.DS.$file);
		}
	}


	/**
	 *
	 * Copy files or dirs
	 */
	public function copyFiles($files, $source_dir, $destination_dir){

		foreach($files as $file){

			// destination is not a source's subfolder?
			if(strpos($destination_dir.DS.$file, $source_dir.DS.$file.DS) !== false){
				continue;
			}

			$this->copyRecursively($source_dir.DS.$file, $destination_dir.DS.$file);
		}

	}

	/**
	 *
	 * Copy files recursively
	 */
	public function copyRecursively($source, $dest){

		// Simple copy for a file
		if (is_file($source)) {
			gator::writeLog('copy - '.$dest);
			return copy($source, $dest);
		}

		// Make destination directory
		if (!is_dir($dest)) {
			mkdir($dest);
		}

		// Loop through the folder
		$dir = dir($source);
		while (false !== $entry = $dir->read()) {
			// Skip pointers
			if ($entry == '.' || $entry == '..') {
				continue;
			}

			// Deep copy directories
			$this->copyRecursively($source.DS.$entry, $dest.DS.$entry);
			gator::writeLog('copy - '.$source.' > '.$dest);
		}

		// Clean up
		$dir->close();

		return true;
	}


	/**
	 *
	 * Move files or dirs
	 */
	public function moveFiles($files, $source_dir, $destination_dir){

		// batch move
		foreach($files as $file){

			if ($file == '.' || $file == '..' || in_array($file, gatorconf::get('restricted_files'))) return false;
			
			if (!file_exists($destination_dir.DS.$file)){

				// destination is not a source's subfolder?
				if(strpos($destination_dir.DS.$file, $source_dir.DS.$file.DS) !== false){
					continue;
				}

				rename($source_dir.DS.$file, $destination_dir.DS.$file);

				gator::writeLog('move - '.$file);

				$this->syncBufferFile($source_dir.DS.$file);
			}
		}
		return;
	}


	/**
	 *
	 * Reneme file or dir
	 */
	public function renameFile($old_name, $new_name){

		if (!file_exists($_SESSION['cwd'].DS.$new_name)){
			rename($_SESSION['cwd'].DS.$old_name, $_SESSION['cwd'].DS.$new_name);

			gator::writeLog('rename - '.$old_name.' > '.$new_name);

			$this->syncBufferFile($_SESSION['cwd'].DS.$old_name);
		}

		return;
	}


	/**
	 *
	 * Zip selected files
	 */
	public function zipFiles($input_files, $destination){

		if (!gatorconf::get('use_zip')) return;

		if (!extension_loaded('zip')) {
			gator::error('Zip PHP module is not installed on this server');
			die;
		}

		$destination = $_SESSION['cwd'].DS.$destination;
		if (substr($destination, -4, 4) != '.zip'){
			$destination = $destination.'.zip';
		}

		$zip = new ZipArchive();
		if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
			gator::error('Archive could not be created');
		}

		$startdir = str_replace('\\', '/', $_SESSION['cwd']);

		foreach ($input_files as $source){

			gator::writeLog('zip - '.$source);

			$source = $_SESSION['cwd'].DS.$source;

			$source = str_replace('\\', '/', $source);

			if (is_dir($source) === true)
			{
				$subdir = str_replace($startdir.'/', '', $source) . '/';
				$zip->addEmptyDir($subdir);
					

				$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

				foreach ($files as $file)
				{

					$file = str_replace('\\', '/', $file);

					// Ignore "." and ".." folders
					if( in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) )
					continue;

					if (is_dir($file) === true)
					{
						$zip->addEmptyDir($subdir . str_replace($source . '/', '', $file . '/'));
					}
					else if (is_file($file) === true)
					{
						$zip->addFromString($subdir . str_replace($source . '/', '', $file), file_get_contents($file));
					}
				}
			}
			else if (is_file($source) === true)
			{
				$zip->addFromString(basename($source), file_get_contents($source));

			}
		}

		$zip->close();

		return;
	}


	/**
	 *
	 * UnZip archive
	 */
	public function unzipFile($file){

		if (!gatorconf::get('use_zip')) return;

		if (!extension_loaded('zip')) {
			gator::error('Zip PHP module is not installed on this server');
			die;
		}

		$file = $_SESSION['cwd'].DS.$file;

		$zip = zip_open($file);
		if(!$zip) {gator::error("Unable to proccess file '{$file}'");}

		$e='';

		while($zip_entry=zip_read($zip)) {
			$zdir=dirname(zip_entry_name($zip_entry));
			$zname=zip_entry_name($zip_entry);
			
			// test if restricted file
			if (strpos($zname, implode("", gatorconf::get('restricted_files'))) !== false) continue;

			if(!zip_entry_open($zip,$zip_entry,"r")) {
				$e.="Unable to proccess file '{$zname}' <br />";
				continue;
			}

			gator::writeLog('unzip - '.$zname);

			// create dir if not exist
			if(!is_dir($_SESSION['cwd'].DS.$zdir)){
				mkdir($_SESSION['cwd'].DS.$zdir, gatorconf::get('new_dir_mode'));
			}

			// do create empty dirs
			if(!is_dir($_SESSION['cwd'].DS.$zname) && substr($zname, -1, 1) == "/"){
				mkdir($_SESSION['cwd'].DS.$zname, gatorconf::get('new_dir_mode'));
				continue;
			}

			$zip_fs=zip_entry_filesize($zip_entry);

			// do create empty files
			if(empty($zip_fs)){
				@touch($_SESSION['cwd'].DS.$zname);
				continue;
			}

			$zz=zip_entry_read($zip_entry,$zip_fs);

			$z=fopen($_SESSION['cwd'].DS.$zname,"w");
			fwrite($z,$zz);
			fclose($z);
			zip_entry_close($zip_entry);

		}
		zip_close($zip);

		if ($e != ''){gator::error($e);}

		return;
	}


	/**
	 *
	 * unset buffer if moving/deleting/renaming something inside buffer
	 */
	public function syncBufferFile($file){

		if (isset($_SESSION['buffer']['files'])){
			foreach($_SESSION['buffer']['files'] as $buffer_file){

				if(strpos($_SESSION['buffer']['directory'].DS.$buffer_file, $file) !== false){
					unset($_SESSION['buffer']);
					return;
				}
			}
		}
	}


	/**
	 *
	 * Mark if file is inside buffer, if not return false
	 */
	public function isBuffered($file){

		if (isset($_SESSION['buffer']['files'])){
			foreach($_SESSION['buffer']['files'] as $buffer_file){

				if($_SESSION['buffer']['directory'].DS.$buffer_file == $file){
					return ' buffer-'.$_SESSION['buffer']['type'];
				}
			}
		}

		return false;
	}


	/**
	 *
	 * Push files to buffer
	 */
	public function pushToBuffer($files, $action){

		$_SESSION['buffer']['files'] = $files;
		$_SESSION['buffer']['type'] = $action;
		$_SESSION['buffer']['directory'] = $_SESSION['cwd'];

		gator::writeLog($action);

		return;
	}


	/**
	 *
	 * Paste (copy) files from buffer to current directory
	 */
	public function pasteFromBuffer(){

		if (!isset($_SESSION['buffer']) || !isset($_SESSION['buffer']['files'])) return false;

		if ($_SESSION['buffer']['type'] == 'copy'){

			$this->copyFiles($_SESSION['buffer']['files'], $_SESSION['buffer']['directory'], $_SESSION['cwd']);

		}elseif($_SESSION['buffer']['type'] == 'cut'){

			$this->moveFiles($_SESSION['buffer']['files'], $_SESSION['buffer']['directory'], $_SESSION['cwd']);

		}else{
			return false;
		}

		return true;
	}


	/**
	 *
	 * Remove directory and all sub-content
	 */
	public function recursiveRemoveDirectory($directory, $empty=FALSE)
	{
		if(substr($directory,-1) == DS)
		{
			$directory = substr($directory,0,-1);
		}
		if(!file_exists($directory) || !is_dir($directory))
		{
			return FALSE;
		}elseif(is_readable($directory))
		{
			$handle = opendir($directory);
			while (FALSE !== ($item = readdir($handle)))
			{
				if($item != '.' && $item != '..')
				{
					$path = $directory.DS.$item;
					if(is_dir($path))
					{
						$this->recursiveRemoveDirectory($path);
					}else{
						unlink($path);
					}
				}
			}
			closedir($handle);
			if($empty == FALSE)
			{
				if(!rmdir($directory))
				{
					return FALSE;
				}
			}
		}
		return TRUE;
	}


	/**
	 *
	 * upload files with ajax
	 */
	public function ajaxUpload(){

		if ($_SERVER['REQUEST_METHOD'] != 'POST') die;
		
		require_once gatorconf::get('base_path')."/include/blueimp/server/php/upload.class.php";

		$upload_handler = new UploadHandler();

		header('Pragma: no-cache');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Content-Disposition: inline; filename="files.json"');
		header('X-Content-Type-Options: nosniff');
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: OPTIONS, HEAD, GET, POST, PUT, DELETE');
		header('Access-Control-Allow-Headers: X-File-Name, X-File-Type, X-File-Size');

		$filename = '?';
		if (isset($_FILES['files']['name'])) {
			$filename = $this->filterInput(implode(" ",$_FILES['files']['name']));
			
			if (in_array($filename, gatorconf::get('restricted_files'))) die;
		}

		gator::writeLog('upload file '.$filename);

		$upload_handler->post();
		
		die;
	}


	/**
	 *
	 * authenticate user
	 */
	public function authenticate(){

		$errors = null;

		if(!isset($_SESSION['simple_auth']['username']) || isset($_GET["login"])) {

			session_destroy();
			session_start();

			if(isset($_POST["submit"])) {

				foreach(gatorconf::get('users') as $user_details){
					if($user_details['username'] == $_POST['username'] && $user_details['password'] == $_POST['password']){

						if(!strstr($user_details['permissions'], 'r')
						|| ($user_details['username'] == 'guest' && gatorconf::get('allow_guests') == false)){
							$no_read_access = true;
							break;
						}

						$_SESSION['simple_auth']['permissions'] = $user_details['permissions'];
						$_SESSION['simple_auth']['username'] = $user_details['username'];

						$_SESSION['simple_auth']['cryptsalt'] = gatorconf::get('encryption_salt');

						gator::writeLog('auth ok');

						// reload
						header('Location: '.gatorconf::get('base_url').'?cd=');
						die;
					}
				}

				if (isset($no_read_access)) {
					$errors = '<div class="error">You don\'t have permission to access.</div>';
					gator::writeLog('auth bad - no read access');
					sleep(1);
				}elseif (isset($_POST['username']) && isset($_POST['password']) && ($_POST['username'] != '' || $_POST['password'] != '')) {
					$errors = '<div class="error">Wrong username or password.</div>';
					gator::writeLog('auth bad - wrong username or password');
					sleep(1);
				}else{
					$errors = '<div class="error">Enter username and password.</div>';
					gator::writeLog('auth bad - blank fields');
					sleep(1);
				}
			}


			if (!isset($_GET["login"]) && gatorconf::get('allow_guests') == true){

				$users = gatorconf::get('users');

				foreach(gatorconf::get('users') as $user_details){
					if ($user_details['username'] == 'guest'){

						$_SESSION['simple_auth']['permissions'] = $user_details['permissions'];
						$_SESSION['simple_auth']['username'] = $user_details['username'];

						$_SESSION['simple_auth']['cryptsalt'] = gatorconf::get('encryption_salt');

						gator::writeLog('auth ok');
						// reload
						header('Location: '.gatorconf::get('base_url'));
						die;
					}
				}
			}


			gator::display("header.php");
			gator::display("login.php", array('errors' => $errors));
			gator::display("footer.php");
			exit;
		}
	}


	/**
	 *
	 * Check users permissions
	 */
	public static function checkPermissions($mode){

		if (gatorconf::get('use_auth') != true) return true;


		for($i=0; $i<strlen($mode); $i++) {

			if (strstr($_SESSION['simple_auth']['permissions'], substr($mode, $i, 1)) == false){
				return false;
			}
		}

		return true;
	}


	/**
	 *
	 * get dir listing
	 */
	public function getDirectoryList()
	{
		$directory = $_SESSION['cwd'];
		$parent_directory = str_replace('\\', '/', dirname($directory));

		// check if home dir
		if ($directory == gatorconf::get('repository')) $home = true;

		// create an array to hold directory list
		$dirs = $files = array();

		// open dir and create a handler
		$handler = opendir($directory);

		// if cannot open, reset current dir to home
		if ($handler == false){
			$directory = $_SESSION['cwd'] = gatorconf::get('repository');
			$handler = opendir($directory);
		}

		// open directory and walk through the filenames
		while (false !== ($file = readdir($handler))) {

			// if file isn't this directory or its parent or resticted, add it to the results
			if ($file != "." && $file != ".." && !in_array($file, gatorconf::get('restricted_files'))) {

				if (filetype($directory.DS.$file) == 'dir'){

					$link = str_replace(gatorconf::get('repository'), '', $directory).DS.$file;
					$link = ltrim($link, '/\\');

					$dirs[] = array(
					'name' => $file,
					'crypt' => $this->encrypt($file),
					'link' => $this->encrypt($link),
					'size' => 0,
					'type' => 'dir',
					'time' => filemtime ($directory.DS.$file),
					'buffer' => $this->isBuffered($directory.DS.$file),
					);

				}else{

					$link = gatorconf::get('base_url').str_replace(gatorconf::get('base_path'), '', $directory).DS.$file;

					$files[] = array(
					'name' => $file,
					'crypt' => $this->encrypt($file),
					'size' => $this->formatBytes(filesize($directory.DS.$file)),
					'sizeb' => filesize($directory.DS.$file),
					'type' => $this->getFileType($file),
					'time' => filemtime ($directory.DS.$file),
					// create url from path
					'link' => $link,
					'buffer' => $this->isBuffered($directory.DS.$file),
					);
				}
			}
		}

		// tidy up: close the handler
		closedir($handler);

		// add back link
		if (!isset($home)){

			$link = str_replace(gatorconf::get('repository'), '', $parent_directory);
			$link = ltrim($link, '/\\');

			array_unshift($dirs, array(
					'name' => 'Go Back',
					'crypt' => $this->encrypt('..'),
					'link' => $this->encrypt($link),
					'size' => 0,
					'type' => 'back',
					'time' => 0,
					'buffer' => false,
			));
		}

		// build breadcrumbs
		$breadcrumb = array();
		$next = '';
		$bc = 'Home' . str_replace(gatorconf::get('repository'), '', $directory);
		$bc = explode('/', $bc);
		foreach ($bc as $bclink){
			if ($bclink != 'Home'){
				$next .= DS.$bclink;
				$next = ltrim($next, '/');
			}
			$breadcrumb[$bclink] = gatorconf::get('base_url').'?cd='.$this->encrypt($next);
		}


		// sort files (not dirs)
		switch($_SESSION['sort']['by']){
			case 'date':
				$files = $this->sortByKey($files, 'time');
				break;

			case 'size':
				$files = $this->sortByKey($files, 'sizeb');
				break;

			case 'name':
			default:
				$files = $this->sortByKey($files, 'name', 'SORT_NATURAL');
				break;
		}

		return array('dirs' => $dirs, 'files' => $files, 'breadcrumb' => $breadcrumb);
	}


	/**
	 *
	 * Sort array by key
	 */
	public function sortByKey($array, $key, $flags = false, $order = null) {

		if ($order == null){
			$order = $_SESSION['sort']['order'];
		}

		$result = array();

		$values = array();
		foreach ($array as $id => $value) {
			$values[$id] = isset($value[$key]) ? $value[$key] : '';
		}

		if ($order == 1) {
			@asort($values, $flags);
		}elseif ($order == -1 && $flags == 'SORT_NATURAL') {
			@arsort($values, $flags);
			$values = array_reverse($values, true);
		}
		else{
			arsort($values, $flags);
		}

		foreach ($values as $key => $value) {
			$result[$key] = $array[$key];
		}

		return $result;
	}


	/**
	 *
	 * Return file type based on extension
	 */
	public static function getFileType($filename){

		if (preg_match("/^.*\.(jpg|jpeg|png|gif|bmp)$/i", $filename) != 0){

			return 'image';

		}elseif (preg_match("/^.*\.(zip)$/i", $filename) != 0){

			return 'zip';
		}

		return 'generic';
	}


	/**
	 *
	 * Bytes formatter
	 */
	public function formatBytes($bytes, $precision = 0) {
		$units = array('B', 'KB', 'MB', 'GB', 'TB');

		$bytes = max($bytes, 0);
		$pow = floor(($bytes ? log($bytes) : 0) / log(1024));
		$pow = min($pow, count($units) - 1);

		$bytes /= pow(1024, $pow);

		return round($bytes, $precision) . ' ' . $units[$pow];
	}


	/**
	 *
	 * Helper for reading php.ini settings
	 */
	public static function returnBytes($val) {
		$val = trim($val);
		$last = strtolower($val[strlen($val)-1]);
		switch($last) {
			case 'g':
				$val *= 1024;
			case 'm':
				$val *= 1024;
			case 'k':
				$val *= 1024;
		}
		return $val;
	}
	
	
	/**
	 *
	 * Display fatal errors
	 */
	public static function error($message){

		echo "</script><script>document.write('')</script>";
		echo "<div class=\"warning\"><strong>{$message}</strong></div><a href=\"?logout=1\">Sign in</a>";

		gator::writeLog('error - '.$message);

		die;
	}


	/**
	 *
	 * do some config validation
	 */
	public static function validateConf($config, $param){

		// do some extra work on repository config query
		if ($param == 'repository'){

			// if user is logged in and has a homedir - set it to be repository
			if( isset($_SESSION['simple_auth']['username'])) {
				foreach ($config['users'] as $user_details){
					if ($user_details['username'] == $_SESSION['simple_auth']['username'] && isset($user_details['homedir']) && $user_details['homedir'] != false){
						$config['repository'] = $user_details['homedir'];
						break;
					}
				}
			}

			// error if repository does not exist
			if (!is_dir($config['repository'])){
				gator::error('Directory does not exist: '.$config['repository']);
			}
		}


		// generate random salt
		if ($param == 'encryption_salt' && $config['encryption_salt'] == 'RandomStringABC1234567890'){

			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			$randomString = '';
			for ($i = 0; $i < 20; $i++) {
				$randomString .= $characters[rand(0, strlen($characters) - 1)];
			}

			$config['encryption_salt'] = $randomString;
		}

		// max_filesize check server's conflict
		if ($param == 'max_filesize'){

			$php_post_max_size =  gator::returnBytes(ini_get('post_max_size'));
			$php_upload_max_filesize =  gator::returnBytes(ini_get('upload_max_filesize'));

			if ($config['max_filesize'] > $php_post_max_size || $config['max_filesize'] > $php_upload_max_filesize){
				gator::error('Config param max_filesize is bigger than php server setting: post_max_size = '.$php_post_max_size.', upload_max_filesize = '.$php_upload_max_filesize);

			}
		}


		// convert array to regexp
		if ($config['accept_file_extensions'] == '*' || in_array('*', $config['accept_file_extensions'])){
			$config['accept_file_extensions'] = '/\.+/';
		}else{
			$config['accept_file_extensions'] = '/(\.|\/)('.implode('|', $config['accept_file_extensions']).')$/i';
		}

		// dependencies
		if ($config['use_lightbox_gallery']) $config['allow_file_links'] = true;
		if (!$config['use_auth']) $config['encrypt_filenames'] = false;
		
		// strip trailing slash & forward slashes for fs
		$config['repository'] = rtrim($config['repository'], "/\\");
		$config['repository'] = str_replace('\\', '/', $config['repository']);
		$config['base_url'] = rtrim($config['base_url'], "/\\");

		return $config;
	}


	/**
	 *
	 * encript string
	 */
	public function encrypt($string)
	{
		// test if encryption is off or blank string
		if (gatorconf::get('encrypt_filenames') != true || !isset($_SESSION['simple_auth']['cryptsalt']) || $string == '') return $string;
			
		$key = $_SESSION['simple_auth']['cryptsalt'];
			
		$encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key))));

		// url safe
		$ret = strtr($encrypted, '+/=', '-_~');

		return $ret;
	}

	/**
	 *
	 * decrypt
	 */
	public function decrypt($string)
	{
		// test if encryption is off or blank string
		if (gatorconf::get('encrypt_filenames') != true || !isset($_SESSION['simple_auth']['cryptsalt']) || $string == '') return $string;
			
		$key = $_SESSION['simple_auth']['cryptsalt'];
			
		// clean url safe
		$encrypted = strtr($string, '-_~', '+/=');
			
		$decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($encrypted), MCRYPT_MODE_CBC, md5(md5($key))), "\0");

		return $decrypted;
	}



	/**
	 *
	 * create directory tree html
	 */
	public function getDirectoryTree($path, $skip_files = false, $link_prefix = '') {

		$objects = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($path), RecursiveIteratorIterator::SELF_FIRST);

		$dom = new DomDocument("1.0");

		$li = $dom;
		$ul = $dom->createElement('ul');
		$li->appendChild($ul);
		$el = $dom->createElement('li', 'Home');
		$at = $dom->createAttribute('clink');
		$at->value = $link_prefix;
		$el->appendChild($at);
		$ul->appendChild($el);

		$node = $ul;
		$depth = -1;


		foreach($objects as $object){

			$name = $object->getFilename();
			
			// skip unwanted files
			if ($name == '.' || $name == '..' || in_array($name, gatorconf::get('restricted_files'))) continue;
			
			$path = str_replace('\\', '/', $object->getPathname());
			$isDir = is_dir($path);

			$link = str_replace(gatorconf::get('repository'), '', $path);
			$link = gator::encodeurl(ltrim($link, '/\\'));

			$skip = false;


			if (($isDir == false && $skip_files == true )){
				// skip unwanted files
				$skip = true;
			}elseif($isDir == false){
				// this is regural file, no links here
				$link = '';
			}else{
				// this is dir
				$link = $link;
			}

			if ($objects->getDepth() == $depth){
				//the depth hasnt changed so just add another li
				if (!$skip){
					$el = $dom->createElement('li', $name);
					$at = $dom->createAttribute('clink');
					$at->value = $link_prefix.$this->encrypt($link);
					$el->appendChild($at);
					if (!$isDir) $el->appendChild($dom->createAttribute('isfile'));

					$node->appendChild($el);

				}
			}
			elseif ($objects->getDepth() > $depth){
				//the depth increased, the last li is a non-empty folder
				$li = $node->lastChild;
				$ul = $dom->createElement('ul');
				$li->appendChild($ul);
				if (!$skip){
					$el = $dom->createElement('li', $name);
					$at = $dom->createAttribute('clink');
					$at->value = $link_prefix.$this->encrypt($link);
					$el->appendChild($at);
					if (!$isDir) $el->appendChild($dom->createAttribute('isfile'));

					$ul->appendChild($el);
				}
				$node = $ul;
			}
			else{
				//the depth decreased, going up $difference directories
				$difference = $depth - $objects->getDepth();
				for ($i = 0; $i < $difference; $difference--){
					$node = $node->parentNode->parentNode;
				}
				if (!$skip){
					$el = $dom->createElement('li', $name);
					$at = $dom->createAttribute('clink');
					$at->value = $link_prefix.$this->encrypt($link);
					$el->appendChild($at);
					if (!$isDir) $el->appendChild($dom->createAttribute('isfile'));

					$node->appendChild($el);
				}
			}
			$depth = $objects->getDepth();
		}

		return $dom->saveHtml();
	}


	/**
	 *
	 * write usage to log
	 */
	public static function writeLog($action){

		if (gatorconf::get('write_log') != true) return;

		if (isset($_SESSION['simple_auth']['username'])){

			$user = $_SESSION['simple_auth']['username'];

		}else{
			$user = 'unknown';
		}

		$ip = $_SERVER["REMOTE_ADDR"];

		$log = date('Y-m-d H:i:s')." | IP $ip | $user | $action \n";

		file_put_contents(gatorconf::get('base_path').DS.'usage.log', $log, FILE_APPEND);
	}

}
